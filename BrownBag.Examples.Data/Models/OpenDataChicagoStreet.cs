﻿namespace BrownBag.Examples.Data.Models
{
    /// <summary>
    /// Open Data Chicago Street Model
    /// Implemented with only auto implemented properties
    /// 
    /// Usually these would be in PascalCase like the class name but the library I'm using doesn't support custom naming to my knowledge
    /// </summary>
    public class OpenDataChicagoStreet
    {
        public string suffix_direction { get; set; }
        public string min_address { get; set; }
        public string direction { get; set; }
        public string street { get; set; }
        public string max_address { get; set; }
        public string full_street_name { get; set; }
        public string suffix { get; set; }
    }
}
