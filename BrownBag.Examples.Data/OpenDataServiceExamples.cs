﻿using BrownBag.Examples.Data.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BrownBag.Examples.Data
{
    /// <summary>
    /// Logic for accessing the City of Chicago's Streets List
    /// https://data.cityofchicago.org/resource/i6bp-fvbx.json
    /// </summary>
    public class OpenDataServiceExamples
    {
        public const string EXAMPLE_DATA_BASE_URL = "https://data.cityofchicago.org/resource/";
        public const string EXAMPLE_DATA_URL = "i6bp-fvbx.json?$limit={limit}&$offset={offset}";
        public const string EXAMPLE_DATA_QUERY_PARAM_LIMIT = "limit";
        public const string EXAMPLE_DATA_QUERY_PARAM_OFFSET = "offset";

        /// <summary>
        /// Example 1: Retrieve street data as JSON collection stringified
        /// </summary>
        /// <returns>Returns street data as JSON collection stringified</returns>
        public string E1()
        {
            // .Result will block execution until Task is resolved
            return RetrieveOpenDataChicagoStreetData().Result;
        }

        /// <summary>
        /// Example 2: Retrieve street data (of the first 50 locations) as a table stringified
        /// </summary>
        /// <returns>Returns street data (of the first 50 locations) as a table stringified</returns>
        public string E2()
        {
            // .Result will block execution until Task is resolved and then the outside function is executed
            return CreateStringTableOfStreets(RetrieveOpenDataChicagoStreetData().Result); 
        }

        public IEnumerable<OpenDataChicagoStreet> GetOpenDataChicagoStreets(string data)
        {
            return SimpleJson.DeserializeObject<List<OpenDataChicagoStreet>>(data);
        }

        private string CreateStringTableOfStreets(string response)
        {
            // Usage of Generics here to specify type of Deserialization target
            // It's a collection of street data so a Generic List is used here for convenience but any IEnumerable would be fine
            // Also used a LINQ extension method, .Take() here to limit results to 50
            var deserializedStreets = GetOpenDataChicagoStreets(response).Take(50);

            // When appending many strings together it is recommended to use something like a string builder
            // More info at: http://www.dotnetperls.com/stringbuilder-performance
            var formattedOutput = new StringBuilder();
            foreach (var street in deserializedStreets)
            {
                formattedOutput.Append(string.Format("{0,5}|{1,30}{2}", street.direction, street.full_street_name, Environment.NewLine));
            }
            
            return formattedOutput.ToString();
        }

        /// <summary>
        /// Returns an executable task that performs common data retrieval logic
        /// </summary>
        /// <returns>Executable task that returns street data from service call when resolved</returns>
        public async Task<string> RetrieveOpenDataChicagoStreetData()
        {
            // Create instance of REST Client
            var client = new RestClient(EXAMPLE_DATA_BASE_URL);

            // Create the request object
            var request = new RestRequest(EXAMPLE_DATA_URL, Method.GET);

            // Add query param data to request
            request.AddUrlSegment(EXAMPLE_DATA_QUERY_PARAM_LIMIT, "5000");
            request.AddUrlSegment(EXAMPLE_DATA_QUERY_PARAM_OFFSET, "0");

            // Create a cancellation token to allow cancelling a task if necessary
            var cancellationTokenSource = new CancellationTokenSource();

            // Create the response
            var response = await client.ExecuteTaskAsync(request, cancellationTokenSource.Token);

            // This will only execute upon completion of the awaited code above
            return response.Content;
        }
    }
}
