﻿using System;

namespace BrownBag.Examples.Basics
{
  public class StringExamples
  {
    private string crazyNameManualPropertyField;

    /// <summary>
    /// This is what .NET does for you for free under the covers
    /// </summary>
    public string ManualProperty
    {
      get
      {
        return crazyNameManualPropertyField;
      }

      private set
      {
        crazyNameManualPropertyField = value;
      }
    }

    /// <summary>
    /// Shows that a string acts like a value type in terms of equality comparison
    /// </summary>
    /// <returns>Returns an assertion if strings are the same</returns>
    public string E1()
    {
      string string1 = "Hello World", string2 = "Hello World";

      bool stringsAreEqual = (string1 == string2);

      return string.Format("The strings '{0}' and '{1}' are {2}", string1, string2, stringsAreEqual ? "equal" : "not equal");
    }

    /// <summary>
    /// Shows that strings are immutable
    /// </summary>
    /// <returns>Returns an assertion if strings are immutable</returns>
    public string E2()
    {
      string string1 = "Hello World";

      string string2 = string1;
      string1 = "Bye everyone!";

      string2.Substring(3);

      bool stringsAreImmutable = (string1 != string2) && (string1 == "Bye everyone!") && (string2 == "Hello World");

      return string.Format("The strings '{0}' and '{1}' are {2}", string1, string2, stringsAreImmutable ? "immutable" : "mutable");
    }

    /// <summary>
    /// Returns a clean string to demonstrate a few string functions
    /// </summary>
    /// <returns>Returns a clean string</returns>
    public string E3()
    {
      var originalString = "   ,A,very,messy,string,,,,,here,  \n";
      var formattedString = originalString.Trim(); // Trim removes whitespace from both the front and back of the string
      formattedString = formattedString.Replace("messy", "clean"); // String replacement
      formattedString = string.Join(" ", formattedString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)); // Break up string based on given separator and then recombine using a space as the separator)
      formattedString += "."; // Append a period at the end

      return string.Format("This string has been formatted: {0}", formattedString);
    }
  }
}
