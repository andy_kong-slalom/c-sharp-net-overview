﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownBag.Examples.ConsoleApp.Models
{
    public class PromptData
    {
        public int TypeChoiceValue { get; set; }
        public string MethodChoiceValue { get; set; }
    }
}
