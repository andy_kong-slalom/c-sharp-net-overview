﻿using BrownBag.Examples.Web.Formatters;
using System.Web.Http;

namespace BrownBag.Examples.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Re‌​ferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Globally return WebAPI requests as JSON by default instead of XML (the built-in default)
            config.Formatters.Add(new BrowserJsonFormatter());
        }
    }
}
